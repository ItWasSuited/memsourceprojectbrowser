//
//  FakeNetworkService.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import MemsourceProjectBrowser

class FakeNetworkService: NetworkServing {
    
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    func fetch(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        completion(data, response, error)
    }
    
}
