//
//  FakeTokenService.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import MemsourceProjectBrowser

class FakeTokenService: TokenServing {
    
    var result: Result<String, NetworkObjectServiceError>?
    var callCount = 0
    
    func fetch(completion: @escaping (Result<String, NetworkObjectServiceError>) -> Void) {
        callCount += 1
        if let result = result {
            completion(result)
        }
    }
}
