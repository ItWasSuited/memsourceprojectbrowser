//
//  FakeRequests.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import MemsourceProjectBrowser

struct FakeRequest: UrlRequestConvertible {
    
    let urlRequest: URLRequest
}
