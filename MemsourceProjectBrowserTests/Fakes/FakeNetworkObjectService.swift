//
//  FakeNetworkObjectService.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import MemsourceProjectBrowser

class FakeNetworkObjectService<T: Decodable>: NetworkObjectServing {
    
    var result: Result<T, NetworkObjectServiceError>?
    
    func fetch<T>(request: UrlRequestConvertible, type: T.Type, completion: @escaping (Result<T, NetworkObjectServiceError>) -> Void) where T : Decodable {
        if let result = result {
            completion(result as! Result<T, NetworkObjectServiceError>)
        }
    }

}
