//
//  FakeProjectsService.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import MemsourceProjectBrowser

class FakeProjectsService: ProjectsServing {
    
    var result: Result<[Project], NetworkObjectServiceError>?
    var givenTokens = [String]()
    
    func fetch(token: String, dueInHours: DueInHours, completion: @escaping (Result<[Project], NetworkObjectServiceError>) -> Void) {
        givenTokens.append(token)
        if let result = result {
            completion(result)
        }
    }
    
}
