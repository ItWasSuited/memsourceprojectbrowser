//
//  ProjectsViewModelSpec.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Quick
import Nimble
@testable import MemsourceProjectBrowser

class ProjectsViewModelSpec: QuickSpec {
    
    override func spec() {
        describe("fetchProjects") {
            var result: Result<[Project], FetchProjectsError>?
            var fakeTokenService: FakeTokenService!
            var fakeProjectsService: FakeProjectsService!
            var sut: ProjectsViewModel!
            beforeEach {
                result = nil
                fakeTokenService = FakeTokenService()
                fakeProjectsService = FakeProjectsService()
                sut = ProjectsViewModel(
                    tokenService: fakeTokenService,
                    projectsService: fakeProjectsService
                )
            }
            
            context("when the token service fails") {
                beforeEach {
                    fakeTokenService.result = .failure(.network)
                }
                
                it("calls completion with auth error") {
                    sut.fetchProjects { result = $0 }
                    expect(result?.error).to(equal(.couldNotAuthenticate))
                }
            }
            
            context("when the token service succeeds") {
                beforeEach {
                    fakeTokenService.result = .success("testToken")
                }
                
                it("the project service is called with the returned token") {
                    sut.fetchProjects { _ in }
                    expect(fakeProjectsService.givenTokens.first).to(equal("testToken"))
                }
                
                context("when the projects service fails") {
                    beforeEach {
                        fakeProjectsService.result = .failure(.network)
                    }
                    
                    it("calls completion with project fetching error") {
                        sut.fetchProjects { result = $0 }
                        expect(result?.error).to(equal(.couldNotFetchProjects))
                    }
                }
                
                context("when the projects service fails") {
                    beforeEach {
                        fakeProjectsService.result = .success([])
                    }
                    
                    it("calls completion with the fetched projects") {
                        sut.fetchProjects { result = $0 }
                        expect(result?.value).to(equal([]))
                    }
                }
            }
            
            context("when the token was already retrieved, a second call to fetchProjects") {
                beforeEach {
                    fakeTokenService.result = .success("testToken1")
                    sut.fetchProjects { _ in }
                    fakeTokenService.result = .success("testToken2")
                }
                
                it("does not call again the token service") {
                    sut.fetchProjects { _ in }
                    expect(fakeTokenService.callCount).to(equal(1))
                }
                
                it("calls fetchProjects again with the first token") {
                    sut.fetchProjects { _ in }
                    expect(fakeProjectsService.givenTokens).to(equal(["testToken1", "testToken1"]))
                }
            }
        }
    }
    
}
