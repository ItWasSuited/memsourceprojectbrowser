//
//  ProjectsServiceSpec.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//


import Quick
import Nimble
@testable import MemsourceProjectBrowser

class ProjectsServiceSpec: QuickSpec {
    
    override func spec() {
        describe("fetch") {
            var result: Result<[Project], NetworkObjectServiceError>?
            var fakeNetworkObjectService: FakeNetworkObjectService<ProjectsResponse>!
            var sut: ProjectsServing!
            beforeEach {
                fakeNetworkObjectService = FakeNetworkObjectService()
                sut = ProjectsService(networkObjectService: fakeNetworkObjectService)
            }
            
            context("when the network object service fails") {
                beforeEach {
                    fakeNetworkObjectService.result = .failure(.network)
                }
                
                it("calls completion with the error") {
                    sut.fetch(token: "testToken", dueInHours: .all){ result = $0 }
                    expect(result?.error).to(equal(.network))
                }
            }
            
            context("when the network object service succeeds") {
                beforeEach {
                    fakeNetworkObjectService.result = .success(ProjectsResponse(content: []))
                }

                it("calls completion with the projects") {
                    sut.fetch(token: "testToken", dueInHours: .all) { result = $0 }
                    expect(result?.value).to(equal([]))
                }
            }
        }
    }
    
}
