//
//  RequestSpec.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Quick
import Nimble
@testable import MemsourceProjectBrowser

class RequestSpec: QuickSpec {
    
    override func spec() {
        describe("urlRequest") {
            
            context("getToken") {
                
                it("creates the expected URLRequest") {
                    let credentials = Credentials(username: "testUser", password: "testPass")
                    let urlRequest = Request.getToken(credentials).urlRequest
                    
                    let expectedURL = "https://cloud.memsource.com/web/api/v3/auth/login?userName=testUser&password=testPass"
                    expect(urlRequest.url?.absoluteString).to(equal(expectedURL))
                }
            }
            
            context("getProjects") {
                
                it("when dueInHours is all creates the expected URLRequest") {
                    let token = "TestToken"
                    let urlRequest = Request.getProjects(token, .all).urlRequest
                    
                    let expectedURL = "https://cloud.memsource.com/web/api2/v1/projects?token=TestToken"
                    expect(urlRequest.url?.absoluteString).to(equal(expectedURL))
                }
                
                it("when dueInHours is not all creates the expected URLRequest") {
                    let token = "TestToken"
                    let urlRequest = Request.getProjects(token, .last72).urlRequest
                    
                    let expectedURL = "https://cloud.memsource.com/web/api2/v1/projects?token=TestToken&dueInHours=72"
                    expect(urlRequest.url?.absoluteString).to(equal(expectedURL))
                }
                
            }
        }
    }
    
}
