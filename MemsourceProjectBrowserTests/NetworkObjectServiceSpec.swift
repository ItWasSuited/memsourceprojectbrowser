//
//  NetworkObjectService.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Quick
import Nimble
@testable import MemsourceProjectBrowser

class NetworkObjectServiceSpec: QuickSpec {
    
    override func spec() {
        describe("fetch") {
            var fakeNetworkService: FakeNetworkService!
            let fakeURL = URL(string: "testURL")!
            let fakeRequest = FakeRequest(urlRequest: URLRequest(url: fakeURL))
            var result: Result<TokenResponse, NetworkObjectServiceError>?
            var sut: NetworkObjectServing!
            beforeEach {
                result = nil
                fakeNetworkService = FakeNetworkService()
                sut = NetworkObjectService(
                    networkService: fakeNetworkService
                )
            }
            
            context("when the network fetcher returns error") {
                beforeEach {
                    fakeNetworkService.error = NSError(domain: "", code: 0, userInfo: nil)
                }
                
                it("calls completion with network error") {
                    sut.fetch(request: fakeRequest, type: TokenResponse.self) { result = $0 }
                    expect(result?.error).to(equal(.network))
                }
            }
            
            context("when the network fetcher returns non 200 http status") {
                beforeEach {
                    fakeNetworkService.response = HTTPURLResponse(
                        url: fakeURL, statusCode: 400, httpVersion: "1.1", headerFields: nil)
                }
                
                it("calls completion with unexpected HTTP code error") {
                    sut.fetch(request: fakeRequest, type: TokenResponse.self) { result = $0 }
                    expect(result?.error).to(equal(.unexpectedHTTPCode))
                }
            }
            
            context("when invalid JSON response is received") {
                beforeEach {
                    fakeNetworkService.response = HTTPURLResponse(
                        url: fakeURL, statusCode: 200, httpVersion: "1.1", headerFields: nil)
                    fakeNetworkService.data = "invalidJSON".data(using: .utf8)
                }
                
                it("calls completion with JSON error") {
                    sut.fetch(request: fakeRequest, type: TokenResponse.self) { result = $0 }
                    expect(result?.error).to(equal(.jsonParsing))
                }
            }
            
            context("when correct auth JSON response is received") {
                beforeEach {
                    fakeNetworkService.response = HTTPURLResponse(
                        url: fakeURL, statusCode: 200, httpVersion: "1.1", headerFields: nil)
                    fakeNetworkService.data = authJSONResponse.data(using: .utf8)
                }
                
                it("calls completion with the parsed object") {
                    sut.fetch(request: fakeRequest, type: TokenResponse.self) { result = $0 }
                    expect(result?.value?.token).to(equal("tokenValue"))
                }
            }
        }
    }
    
}

let authJSONResponse = """
{
"user": {
"id": 287392,
"uid": "l9lQjzL12SMZ6qWlF0iXp6",
"firstName": "Emilio",
"lastName": "Fernandez",
"userName": "Emilio Fernandez",
"email": "emilioferasti@gmail.com",
"role": "ADMIN",
"timezone": "Europe/London",
"active": true,
"deleted": false,
"terminologist": false,
"dateCreated": "2018-09-03T12:41:28+0000"
},
"token": "tokenValue",
"expires": "2018-09-04T22:35:15+0000"
}
"""
