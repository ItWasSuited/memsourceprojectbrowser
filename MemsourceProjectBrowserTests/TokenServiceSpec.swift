//
//  TokenServiceSpec.swift
//  MemsourceProjectBrowserTests
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Quick
import Nimble
@testable import MemsourceProjectBrowser

class TokenServiceSpec: QuickSpec {
    
    override func spec() {
        describe("fetch") {
            var result: Result<String, NetworkObjectServiceError>?
            let credentials = Credentials(username: "testUser", password: "testPass")
            let fakeCredentialsService = FakeCredentialsService(credentials: credentials)
            var fakeNetworkObjectService: FakeNetworkObjectService<TokenResponse>!
            var sut: TokenServing!
            beforeEach {
                fakeNetworkObjectService = FakeNetworkObjectService()
                sut = TokenService(
                    credentialsService: fakeCredentialsService,
                    networkObjectService: fakeNetworkObjectService
                )
            }
            
            context("when the network object service fails") {
                beforeEach {
                    fakeNetworkObjectService.result = .failure(.network)
                }
                
                it("calls completion with the error") {
                    sut.fetch { result = $0 }
                    expect(result?.error).to(equal(.network))
                }
            }
            
            context("when the network object service succeeds") {
                beforeEach {
                    fakeNetworkObjectService.result = .success(TokenResponse(token: "testToken"))
                }
                
                it("calls completion with the token") {
                    sut.fetch { result = $0 }
                    expect(result?.value).to(equal("testToken"))
                }
            }
        }
    }
    
}
