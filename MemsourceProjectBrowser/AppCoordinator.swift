//
//  AppCoordinator.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 03/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

class AppCoordinator: ProjectsViewControllerDelegate {
    
    private let window: UIWindow
    private let projectsViewModel: ProjectsViewModel
    private var navigationController: UINavigationController?
    
    init(window: UIWindow, projectsViewModel: ProjectsViewModel) {
        self.window = window
        self.projectsViewModel = projectsViewModel
    }
    
    func start() {
        let projectsViewController = ProjectsViewController(viewModel: projectsViewModel)
        projectsViewController.delegate = self
        navigationController = UINavigationController(rootViewController: projectsViewController)
        window.rootViewController = navigationController!
        window.makeKeyAndVisible()
    }
    
    func didSelect(project: Project) {
        let detailViewController = ProjectDetailViewController(project: project)
        navigationController!.pushViewController(detailViewController, animated: true)
    }
    
}
