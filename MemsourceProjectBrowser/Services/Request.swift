//
//  URLRequestBuilder.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

protocol UrlRequestConvertible {
    
    var urlRequest: URLRequest { get }
}

enum Request: UrlRequestConvertible {
    case getToken(Credentials)
    case getProjects(String, DueInHours)
    
    var urlRequest: URLRequest {
        get {
            let baseURLString = "https://cloud.memsource.com"
            let path: String
            let queryItems: [URLQueryItem]
            switch self {
            case .getToken(let credentials):
                path = "/web/api/v3/auth/login"
                queryItems = [
                    URLQueryItem(name: "userName", value: credentials.username),
                    URLQueryItem(name: "password", value: credentials.password),
                ]
            case .getProjects(let token, let dueInHours):
                path = "/web/api2/v1/projects"
                if let hours = dueInHours.hours {
                    queryItems = [
                        URLQueryItem(name: "token", value: token),
                        URLQueryItem(name: "dueInHours", value: String(describing: hours)),
                    ]
                }
                else {
                    queryItems = [
                        URLQueryItem(name: "token", value: token),
                        
                    ]
                }
            }
            
            var urlComponents = URLComponents(string: baseURLString)!
            urlComponents.path = path
            urlComponents.queryItems = queryItems
            
            let url = urlComponents.url!
            return URLRequest(url: url)
        }
    }
}
