//
//  NetworkService.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import os

protocol NetworkServing {
    /// Fetches the given request and calls the given completion block after the request finishes.
    /// The purpose of this protocol is just to hide the URLSession calls.
    func fetch(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

struct NetworkService: NetworkServing {
    
    func fetch(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        os_log("Sending request to URL %@",  log: .default, type: .info, request.url!.absoluteString)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { data, response, error in
            completion(data, response, error)
        }
        task.resume()
    }
}
