//
//  CredentialsService.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

struct Credentials: Decodable, Equatable {
    let username: String
    let password: String
}

protocol CredentialsServing {
    var credentials: Credentials { get }
}

struct CredentialsService: CredentialsServing {
    var credentials: Credentials {
        guard
            let url = Bundle.main.url(forResource: "credentials.json", withExtension: nil),
            let file = try? FileHandle(forReadingFrom: url)
        else {
            fatalError("Could not find credentials.json! Are you sure it exists?")
        }
        
        let data = file.readDataToEndOfFile()
        guard let credentials = try? JSONDecoder().decode(Credentials.self, from: data) else {
            fatalError("Could not read credentials! The format should be {\"username\": \"yourUsername\"}, \"password\": \"yourPass\"}")
        }
        
        return credentials
    }
    
}
