//
//  ProjectsService.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 03/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

struct Project: Codable, Equatable {
    let name: String
    let status: String
    let sourceLang: String
    let targetLangs: [String]
}

struct ProjectsResponse: Codable {
    let content: [Project]
}

protocol ProjectsServing {
    func fetch(token: String, dueInHours: DueInHours, completion: @escaping (Result<[Project], NetworkObjectServiceError>) -> Void)
}

struct ProjectsService: ProjectsServing {
    
    let networkObjectService: NetworkObjectServing
    
    func fetch(token: String, dueInHours: DueInHours, completion: @escaping (Result<[Project], NetworkObjectServiceError>) -> Void) {
        let request = Request.getProjects(token, dueInHours)
        networkObjectService.fetch(request: request, type: ProjectsResponse.self) {
            switch $0 {
            case .success(let projectsResponse):
                completion(.success(projectsResponse.content))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
