//
//  NetworkObjectService.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import os

enum NetworkObjectServiceError: Error {
    case network
    case unexpectedHTTPCode
    case jsonParsing
}

protocol NetworkObjectServing {
    func fetch<T: Decodable>(request: UrlRequestConvertible, type: T.Type, completion: @escaping (Result<T, NetworkObjectServiceError>) -> Void)
}

struct NetworkObjectService: NetworkObjectServing {
    
    let networkService: NetworkServing
    
    func fetch<T: Decodable>(request: UrlRequestConvertible, type: T.Type, completion: @escaping (Result<T, NetworkObjectServiceError>) -> Void) {
        networkService.fetch(request: request.urlRequest) { data, response, error in
            guard error == nil else {
                os_log("Network error",  log: .default, type: .info)
                completion(.failure(.network))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                fatalError("Unexpected URL response")
            }
            
            let httpStatusOK = 200
            guard response.statusCode == httpStatusOK else {
                completion(.failure(.unexpectedHTTPCode))
                return
            }
            
            guard let object = try? JSONDecoder().decode(T.self, from: data!) else {
                os_log("Could not decode JSON",  log: .default, type: .info)
                completion(.failure(.jsonParsing))
                return
            }
            
            completion(.success(object))
        }
    }
}
