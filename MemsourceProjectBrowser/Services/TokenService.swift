//
//  TokenService.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import os

struct TokenResponse: Codable {
    let token: String
}

protocol TokenServing {
    func fetch(completion: @escaping (Result<String, NetworkObjectServiceError>) -> Void)
}

struct TokenService: TokenServing {
    
    let credentialsService: CredentialsServing
    let networkObjectService: NetworkObjectServing
    
    func fetch(completion: @escaping (Result<String, NetworkObjectServiceError>) -> Void) {
        let credentials = credentialsService.credentials
        let request = Request.getToken(credentials)
        networkObjectService.fetch(request: request, type: TokenResponse.self) {
            switch $0 {
            case .success(let tokenResponse):
                os_log("Received token: %@",  log: .default, type: .info, tokenResponse.token)
                completion(.success(tokenResponse.token))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
