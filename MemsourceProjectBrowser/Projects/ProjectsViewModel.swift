//
//  ProjectsViewModel.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

enum FetchProjectsError: Error {
    case couldNotAuthenticate
    case couldNotFetchProjects
}

enum DueInHours: Int {
    case all
    case last4
    case last8
    case last24
    case last72
    
    var text: String {
        get {
            switch self {
            case .all:
                return "All"
            case .last4:
                return "4"
            case .last8:
                return "8"
            case .last24:
                return "24"
            case .last72:
                return "72"
            }
        }
    }
    
    var hours: Int? {
        get {
            switch self {
            case .all:
                return nil
            case .last4:
                return 4
            case .last8:
                return 8
            case .last24:
                return 24
            case .last72:
                return 72
            }
        }
    }
}

class ProjectsViewModel {
    
    private let tokenService: TokenServing
    private let projectsService: ProjectsServing
    private var token: String? = nil
    let dueInHoursOptions: [DueInHours] = [.all, .last4, .last8, .last24, .last72]
    
    init(tokenService: TokenServing, projectsService: ProjectsServing) {
        self.tokenService = tokenService
        self.projectsService = projectsService
    }
    
    var dueInHours: DueInHours = .all
    
    func fetchProjects(completion: @escaping (Result<[Project], FetchProjectsError>) -> Void) {
        if let token = token {
            fetchProjects(token: token, completion: completion)
        }
        else {
            tokenService.fetch { [weak self] in
                switch $0 {
                case .success(let token):
                    self?.token = token
                    self?.fetchProjects(token: token, completion: completion)
                case .failure(_):
                    completion(.failure(.couldNotAuthenticate))
                }
            }
        }
    }
    
    private func fetchProjects(token: String, completion: @escaping (Result<[Project], FetchProjectsError>) -> Void) {
        projectsService.fetch(token: token, dueInHours: self.dueInHours) {
            switch $0 {
            case .success(let projects):
                completion(.success(projects))
            case .failure(_):
                completion(.failure(.couldNotFetchProjects))
            }
        }
    }
    
}

extension Project {
    var targetLanguagesString : String {
        get {
            return targetLangs.joined(separator: ", ")
        }
    }
}
