//
//  ProjectsViewController.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 03/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

protocol ProjectsViewControllerDelegate: class {
    func didSelect(project: Project)
}

class ProjectsViewController: UITableViewController, FilterViewDelegate {
    
    let viewModel: ProjectsViewModel
    var projects: [Project]?
    weak var delegate: ProjectsViewControllerDelegate?
    
    lazy var filterView: FilterView = {
        let filterViewNib = UINib(nibName: "FilterView", bundle: nil)
        let filterView = filterViewNib.instantiate(withOwner: nil, options: nil).first as! FilterView
        filterView.dueInHoursOptions = viewModel.dueInHoursOptions
        filterView.delegate = self
        
        return filterView
    }()
    
    init(viewModel: ProjectsViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Projects"
        
        tableView.rowHeight = 73
        
        let cellNib = UINib(nibName: "ProjectTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: ProjectTableViewCell.cellId)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(ProjectsViewController.fetchProjects)
        )
        
        fetchProjects()
    }
    
    @objc private func fetchProjects() {
        viewModel.fetchProjects() { [weak self] result in
            DispatchQueue.main.async { [weak self] in
                self?.handleFetchProjectsResult(result: result)
            }
        }
    }
    
    private func handleFetchProjectsResult(result: Result<[Project], FetchProjectsError>) {
        switch result {
        case .success(let projects):
            self.projects = projects
            tableView.reloadData()
        case .failure:
            showAlert(message: "Could not fetch projects!")
        }
    }
    
    private func showAlert(message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! ProjectTableViewCell
        
        cell.project = projects![indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return filterView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 151
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate!.didSelect(project: projects![indexPath.row])
    }
    
    // MARK: - FilterView delegate
    
    func filterViewDidSelectIndex(index: Int) {
        let dueInHours = viewModel.dueInHoursOptions[index]
        viewModel.dueInHours = dueInHours
        fetchProjects()
    }

}
