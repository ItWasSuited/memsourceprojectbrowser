//
//  FilterView.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

protocol FilterViewDelegate: class {
    func filterViewDidSelectIndex(index: Int)
}

class FilterView: UIView, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var delegate: FilterViewDelegate?
    var dueInHoursOptions: [DueInHours]? {
        didSet {
            for (index, x) in dueInHoursOptions!.enumerated() {
                segmentedControl.setTitle(x.text, forSegmentAt: index)
            }
            
            pickerView.delegate = self
            pickerView.dataSource = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        segmentedControl.addTarget(self, action: #selector(FilterView.segmentedControlChanged), for: .valueChanged)
        
        segmentedControl.selectedSegmentIndex = 0
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    @objc func segmentedControlChanged() {
        let index = segmentedControl.selectedSegmentIndex
        selectIndex(index: index)
    }
    
    private func selectIndex(index: Int) {
        segmentedControl.selectedSegmentIndex = index
        pickerView.selectRow(index, inComponent: 0, animated: true)
        
        delegate!.filterViewDidSelectIndex(index: index)
    }
    
    // MARK: - PickerView delegate & datasource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dueInHoursOptions!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dueInHoursOptions![row].text
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectIndex(index: row)
    }
}
