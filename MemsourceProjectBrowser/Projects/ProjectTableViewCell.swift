//
//  ProjectTableViewCell.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 03/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    static let cellId = "CellId"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sourceLanguageLabel: UILabel!
    @IBOutlet weak var targetLanguagesLabel: UILabel!
    
    var project: Project? {
        didSet {
            nameLabel.text = project!.name
            statusLabel.text = project!.status
            sourceLanguageLabel.text = project!.sourceLang
            targetLanguagesLabel.text = project!.targetLanguagesString
        }
    }

}
