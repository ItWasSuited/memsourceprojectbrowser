//
//  Result.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
    
    var value: T? {
        switch self {
        case .success(let v):
            return v
        case .failure(_):
            return nil
        }
    }
    
    var error: E? {
        switch self {
        case .success(_):
            return nil
        case .failure(let e):
            return e
        }
    }
    
}
