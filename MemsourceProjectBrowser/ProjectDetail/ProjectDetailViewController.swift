//
//  ProjectDetailViewController.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

class ProjectDetailViewController: UIViewController {
    
    let project: Project
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sourceLanguageLabel: UILabel!
    @IBOutlet weak var targetLanguagesLabel: UILabel!
    
    init(project: Project) {
        self.project = project
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = project.name
        statusLabel.text = project.status
        sourceLanguageLabel.text = project.sourceLang
        targetLanguagesLabel.text = project.targetLanguagesString
    }
    
}
