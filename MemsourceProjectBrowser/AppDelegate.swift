//
//  AppDelegate.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 03/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if NSClassFromString("XCTestCase") != nil {
            return true
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let resolver = AppResolver(window: window!)
        appCoordinator = resolver.resolveAppCoordinator()
        appCoordinator.start()
        
        return true
    }

}

