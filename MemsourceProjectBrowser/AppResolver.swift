//
//  AppResolver.swift
//  MemsourceProjectBrowser
//
//  Created by Emilio Fernandez Astigarraga on 04/09/2018.
//  Copyright © 2018 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

struct AppResolver {
    
    let window: UIWindow
    
    func resolveAppCoordinator() -> AppCoordinator {
        let networkObjectService = NetworkObjectService(networkService: NetworkService())
        let projectsService = ProjectsService(networkObjectService: networkObjectService)
        let credentialsService = CredentialsService()
        let tokenService = TokenService(credentialsService: credentialsService, networkObjectService: networkObjectService)
        let projectsViewModel = ProjectsViewModel(tokenService: tokenService, projectsService: projectsService)
        return AppCoordinator(window: window, projectsViewModel: projectsViewModel)
    }
}
