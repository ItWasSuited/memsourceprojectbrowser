Memsource Project Browser app

In order for this to work, you should create a file called credentials.json containing your credentials. Check CredentialsService.swift for more info.

System requirements:
- Xcode 9.4.1
- Cocoapods (I have version 1.5.3).
